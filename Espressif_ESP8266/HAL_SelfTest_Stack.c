/* *****************************************************************************
* File Name: SelfTest_Stack.c
* Version 1.1
*
* Description:
*  This file provides the source code to the API for runtime Stack self tests for 
*  PSoC 4
*
* Owner:
*  JOBI
* 
* Related Document: 
*  AN89056: PSoC 4 - IEC 60730 Class B and IEC 61508 Safety Software Library
*
* Hardware Dependency:
*  CY8C40XX, CY8C42XX, CY8C42XXM, CY8C42XXL, CY8C40XXS, CY8C41XXS and 
*  CY8C41XXS Plus Devices
*******************************************************************************/

#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266

/********************************* includes **********************************/
#include "HAL_SelfTest_Stack.h"
//#include "TestConfiguration.h"

#ifdef HAL_SELFTEST_STACK

/***************************** defines / macros ******************************/



/************************ local data type definitions ************************/

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/


/****************************** local functions ******************************/


/************************ externally visible functions ***********************/
//********************************************************************************
/*!
\author     Kraemer E
\date       29.11.2020
\brief      This function initializes the upper stack area with 0xAA and 0x55 pattern.
\return     none
\param      none
***********************************************************************************/
void HAL_SelfTest_Stack_Init(void)
{
	
}

//********************************************************************************
/*!
\author     Kraemer E
\date       29.11.2020
\brief      This function performs stack self test. It checks upper stack area for 0xAA 
            and 0x55 pattern.
\return     Result of test:  "0" - pass test; "1" - fail test.
\param      none
***********************************************************************************/
u8 HAL_SelfTest_Stack_Check(void)
{
	u8 ucRetVal = OK_STATUS;	
	return ucRetVal;
}

#endif //#ifdef HAL_SELFTEST_STACK

#endif //ESPRESSIF_ESP8266