/* *****************************************************************************
* File Name: SelfTest_Stack.c
* Version 1.1
*
* Description:
*  This file provides the source code to the API for runtime Stack self tests for 
*  PSoC 4
*
* Owner:
*  JOBI
* 
* Related Document: 
*  AN89056: PSoC 4 - IEC 60730 Class B and IEC 61508 Safety Software Library
*
* Hardware Dependency:
*  CY8C40XX, CY8C42XX, CY8C42XXM, CY8C42XXL, CY8C40XXS, CY8C41XXS and 
*  CY8C41XXS Plus Devices
*******************************************************************************/
#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266
    
/********************************* includes **********************************/
#include "BaseTypes.h"
#include "HAL_SelfTest_TimeBase.h"
#include "HAL_Timer.h"
#include "HAL_Watchdog.h"

#ifdef HAL_SELFTEST_TIMEBASE

/***************************** defines / macros ******************************/

    
/************************ local data type definitions ************************/

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/
    
/************************ export data (const and var) ************************/


/****************************** local functions ******************************/

/************************ externally visible functions ***********************/

//********************************************************************************
/*!
\author     Kraemer E
\date       28.11.2020
\brief      Timebase test. This test shall only be tested on start-up because
            it will consume 25ms test time.
\return     OK_STATUS, ERROR_STATUS
\param      none
***********************************************************************************/
u8 HAL_SelfTest_TimeBase_StartUp(void)
{    
    return OK_STATUS;
}
//********************************************************************************
/*!
\author     Kraemer E
\date       28.11.2020
\brief      Initialize the counts for the next test step.
\return     OK_STATUS
\param      none
***********************************************************************************/
u8 HAL_SelfTest_TimeBase_CyclicInit(void)
{
    return OK_STATUS;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       28.11.2020
\brief      This test is called cyclical and checks for correct time bases.
\return     OK_STATUS, ERROR_STATUS, PENDING_STATUS
\param      none
***********************************************************************************/
u8 HAL_SelfTest_TimeBase_CyclicTest(void)
{
    u8 ucTestResult = OK_STATUS;
    return ucTestResult;
}
#endif //#ifdef HAL_SELFTEST_TIMEBASE

#endif //ESPRESSIF_ESP8266