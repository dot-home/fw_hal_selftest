/* *****************************************************************************
* File Name: HAL_SelfTest_CPU.c
* Version 1.1
*
* Description:
*  This file provides the source code to the API for runtime memory self tests for 
*  PSoC 4

* Related Document: 
*  AN89056: PSoC 4 - IEC 60730 Class B and IEC 61508 Safety Software Library
*
* Hardware Dependency:
*  CY8C40XX, CY8C42XX, CY8C42XXM, CY8C42XXL, CY8C40XXS, CY8C41XXS and 
*  CY8C41XXS Plus Devices
*******************************************************************************/

#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266

/********************************* includes **********************************/
#include "HAL_SelfTest_CPU.h"
#include "BaseTypes.h"

#ifdef HAL_SELFTEST_CPU
    
/***************************** defines / macros ******************************/

/************************ local data type definitions ************************/

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/


/****************************** local functions ******************************/

/************************ externally visible functions ***********************/
//********************************************************************************
/*!
\author  Kraemer E
\date    13.04.2021
\brief  
\param   none
\return  none
***********************************************************************************/
u8 HAL_SelfTest_CPU_PC(void)
{     
    u8 uRet = ERROR_STATUS;    //Is equal to eSelfTest_ERROR
    uRet = OK_STATUS; //Is equal to eSelfTest_OK
    return uRet;	
}


//********************************************************************************
/*!
\author  Kraemer E
\date    13.04.2021
\brief   Test the CPU register. This function performs checkerboard test for 
         all CPU registers except the PC register.
\param   none
\return  ucReturnValue - "0" - pass test; "1" - fail test.
***********************************************************************************/
u8 HAL_SelfTest_CPU_Reg(void)
{
    u8 ucReturnValue = OK_STATUS;    
    return ucReturnValue;	
}

#endif 

#endif //ESPRESSIF_ESP8266
