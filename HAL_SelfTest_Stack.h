/*******************************************************************************
* File Name: SelfTest_Stack.h
*  Version 1.1
*
* Description:
*  This file provides constants and parameter values used for Stack    
*  self tests for PSoC 4
*
* Owner:
*  JOBI
* 
* Related Document: 
*  AN89056: PSoC 4 - IEC 60730 Class B and IEC 61508 Safety Software Library
*
* Hardware Dependency:
*  CY8C40XX, CY8C42XX, CY8C42XXM, CY8C42XXL, CY8C40XXS, CY8C41XXS and 
*  CY8C41XXS Plus Devices
*******************************************************************************/
#ifndef _HAL_SELFTEST_STACK_H_
#define _HAL_SELFTEST_STACK_H_

#include "BaseTypes.h"
#include "SelfTest_Config.h"
    
#ifdef HAL_SELFTEST_STACK

/*************************************** 
* Function Prototypes 
***************************************/
void  HAL_SelfTest_Stack_Init(void);
u8    HAL_SelfTest_Stack_Check(void);


/* Status flags to return function results */
#if !defined(ERROR_STATUS)
    #define ERROR_STATUS  	           (1u)
#endif /* End ERROR_STATUS) */

#if !defined(OK_STATUS)
    #define OK_STATUS   		       (0u)
#endif /* End OK_STATUS) */

#endif //#ifdef HAL_SELFTEST_STACK

#endif /* _HAL_SELFTEST_STACK_H_ */


/* END OF FILE */
