/* *****************************************************************************
* File Name: SelfTest_Stack.c
* Version 1.1
*
* Description:
*  This file provides the source code to the API for runtime Stack self tests for 
*  PSoC 4
*
* Owner:
*  JOBI
* 
* Related Document: 
*  AN89056: PSoC 4 - IEC 60730 Class B and IEC 61508 Safety Software Library
*
* Hardware Dependency:
*  CY8C40XX, CY8C42XX, CY8C42XXM, CY8C42XXL, CY8C40XXS, CY8C41XXS and 
*  CY8C41XXS Plus Devices
*******************************************************************************/

#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS

/********************************* includes **********************************/
#include "CyLib.h"
#include <project.h>
#include "HAL_SelfTest_UART.h"
#include "BaseTypes.h"

#ifdef HAL_SELFTEST_UART
//#include "TestConfiguration.h"

/***************************** defines / macros ******************************/
/***************************************
* Initial Parameter Constants 
***************************************/

/* UART data transmit guard interval using guard interval > */
/* (uart_bitrate * uart_data_bits(start data parity stop) * uart_tx_buf_size) */
#define UART_TX_DATA_TIME              (400u)
#define UART_RX_DATA_TIME			   (400u)

#define UART_SCB_TRANSMIT_BYTE_ERROR    100u
#define UART_TEST_RANGE					0xF0u

/************************ local data type definitions ************************/

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/


/****************************** local functions ******************************/
//********************************************************************************
/*!
\author     Kraemer E
\date       25.04.2021
\brief      This function tests the internal loopback of the UART.
            Transmitted byte must by equal to received byte.
\return     ucRet - The result if this test
\param      ucTxByte - Data to be transmit over UART
***********************************************************************************/
static u8 TransmitByte(uint8 ucTxByte)
{
    u16 uiGuardCnt = 0u;
    u8  ucRet = OK_STATUS;
        
    /* Check if intentional error should be made for testing */        
    #if (ERROR_IN_UART_SCB == 1u)
		if (ucTxByte == UART_TRANSMIT_BYTE_ERROR)
        {
            /* Change value for UART transferring to unexpected  */
            ucTxByte++;
        }
    #endif /* End (ERROR_IN_UART_SCB == 1u) */
        
  	/* Transmit byte */
	UART_UartPutChar(ucTxByte);
    
    /* Check if intentional error should be made for testing */        
    #if (ERROR_IN_UART_SCB == 1U)
        if(ucTxByte == (UART_TRANSMIT_BYTE_ERROR + 1u))
        {
            /* Restore expected value */
            ucTxByte--;
        }
    #endif /* End (ERROR_IN_UART_SCB == 1u) */    
    
	/* Wait while byte will be sent */   
	/* Use guard interval > (spi_bitrate * spi_data_bits * spi_tx_buf_size) */
	do
	{
		uiGuardCnt++;
		CyDelayUs(1u);
	} while( (0u != (UART_SpiUartGetTxBufferSize() + UART_GET_TX_FIFO_SR_VALID)) && (uiGuardCnt < UART_TX_DATA_TIME));
	
    /* If timeout occurs */
	if (uiGuardCnt >= UART_TX_DATA_TIME)
	{
		ucRet = ERROR_STATUS;
	}  
	else
	{		
		/* Reset GuardCnt */
		uiGuardCnt = 0u;
		
		do
		{
			uiGuardCnt++;
			CyDelayUs(10u);
		} 
        while( (0u == (UART_SpiUartGetRxBufferSize() + UART_GET_RX_FIFO_SR_VALID)) && (uiGuardCnt < UART_RX_DATA_TIME));
		
		/* If timeout, error status is returned */
		if (uiGuardCnt >= UART_RX_DATA_TIME)
		{
			ucRet = ERROR_STATUS;
		}
		else
		{
			/* Check if received and transmitted values are the same */
			if(ucTxByte != ((uint8)UART_UartGetChar()))
			{  
	        	/* If not - return ERROR status */     
				ucRet = ERROR_STATUS;
			}
		}
	}
    
    /* Return result */ 
	return ucRet;
}


/************************ externally visible functions ***********************/
//********************************************************************************
/*!
\author     Kraemer E
\date       25.04.2021
\brief      This function initializes the UART module and clears the buffers
\return     none
\param      none
***********************************************************************************/
void SelfTest_UART_Init(void)
{
	/* Initialize UART component */
    UART_Start();
      
    #ifdef CY_SMARTIO_SmartIO_1_H
        /* Enable SmartIO Module */
        SmartIO_1_Start();
        
        /* Bypass all the SmartIO configuration for normal mode */
        SmartIO_1_SetBypass(SmartIO_1_CHANNEL_ALL);
        
        CyDelayUs(100);
    #endif
    
    /* Clear RX and TX Buffers */
    UART_SpiUartClearRxBuffer();
    UART_SpiUartClearTxBuffer(); 
}

//********************************************************************************
/*!
\author     Kraemer E
\date       25.04.2021
\brief      This function tests the internal loopback of the UART.
            Transmitted byte must by equal to received byte.
            During call, function transmits and receives bytes from 0x01 to 0xFF 
\return     ucRetVal - Result of test:  "0" - pass test; "1" - fail test.
\param      none
***********************************************************************************/
u8 SelfTest_UART_Check(void)
{
	u8 ucRetVal = OK_STATUS;
		
    /* Init byte for testing */
    static u8 ucTestByte = 5;
    u16 uiGuardCnt = 0;
    u32 ulRxUartIsrMask = 0;
    u32 ulTxUartIsrMask = 0;
    
    /* Check if RX and TX are enabled */
    #if (UART_UART_TX_RX != UART_UART_DIRECTION)
        /* Return error */
        ucRetVal = ERROR_STATUS;        
    #else
        
        /* Wait for end of user receiving. Don't test if the buffer contains data */
        while( ((UART_SpiUartGetRxBufferSize() + UART_GET_RX_FIFO_SR_VALID) != 0u) && uiGuardCnt < UART_RX_DATA_TIME)
        {
            uiGuardCnt++;
            CyDelayUs(1u);
        }       
               	
        /* If timeout */
    	if (uiGuardCnt >= UART_RX_DATA_TIME)
    	{
    		ucRetVal = ERROR_RX_NOT_EMPTY;
    	}
    	else
    	{
            
        	/* Wait for end of user transmitting and don't test if the buffer contains data */
        	/* Use guard interval > (spi_bitrate * spi_data_bits * spi_tx_buf_size) */
        	do
        	{
        		uiGuardCnt++;
        		CyDelayUs(1u);
        	} while ((0u != (UART_SpiUartGetTxBufferSize() + UART_GET_TX_FIFO_SR_VALID)) && (uiGuardCnt < UART_TX_DATA_TIME));
    	        
    	    /* If timeout */
        	if (uiGuardCnt >= UART_TX_DATA_TIME)
        	{
        		ucRetVal = ERROR_TX_NOT_EMPTY;
        	}	
    		else
    		{            
    	    	/* Turn on loopback by disabling the bypass on all the SmartIO configuration for test mode */
                SmartIO_1_SetBypass(SmartIO_1_CHANNEL_NONE);
                
    	        /* Clear RX, TX buffers */    
    		    UART_SpiUartClearRxBuffer();
                UART_SpiUartClearTxBuffer(); 
                
    	        /* Disable RX and TX interrupts befor the test */
                ulRxUartIsrMask = UART_GetRxInterruptMode();
                UART_SetRxInterrupt(UART_NO_INTR_SOURCES);
    			
                ulTxUartIsrMask = UART_GetTxInterruptMode();
                UART_SetTxInterrupt(UART_NO_INTR_SOURCES);

    	      	/* Transmit bytes from 0x01 to 0xFF*/
    	        ucRetVal = TransmitByte(ucTestByte);            
    		        			        
    	        /* Enabled RX and TX interrupts after the test */    
    			UART_SetRxInterrupt(ulRxUartIsrMask);			
                UART_SetTxInterrupt(ulTxUartIsrMask);
    		    
    	    	/* Turn off loopback by enabling the bypass on all the SmartIO configuration for normal mode */
                SmartIO_1_SetBypass(SmartIO_1_CHANNEL_ALL);
    			
    			/* Increment index */
    			ucTestByte++;  
    			if (ucTestByte >= UART_TEST_RANGE)
    			{
    				ucTestByte = 0u;
    			}
            }
    	}

    	/* Need to clear buffer after MUX switch */
    	CyDelayUs(100u);
    	UART_SpiUartClearRxBuffer();
        UART_SpiUartClearTxBuffer(); 
    #endif  
		
	return ucRetVal;
}

#endif

#endif //PSOC_4100S_PLUS