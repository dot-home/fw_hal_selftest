/* *****************************************************************************
* File Name: SelfTest_Stack.c
* Version 1.1
*
* Description:
*  This file provides the source code to the API for runtime Stack self tests for 
*  PSoC 4
*
* Owner:
*  JOBI
* 
* Related Document: 
*  AN89056: PSoC 4 - IEC 60730 Class B and IEC 61508 Safety Software Library
*
* Hardware Dependency:
*  CY8C40XX, CY8C42XX, CY8C42XXM, CY8C42XXL, CY8C40XXS, CY8C41XXS and 
*  CY8C41XXS Plus Devices
*******************************************************************************/
#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS
    
/********************************* includes **********************************/
#include "CyLib.h"
#include "BaseTypes.h"
#include "HAL_SelfTest_TimeBase.h"
#include "HAL_Timer.h"
#include "HAL_Watchdog.h"
#include "HAL_System.h"
#ifdef HAL_SELFTEST_TIMEBASE

/***************************** defines / macros ******************************/
#define SET_ERROR_IN_INTERRUPT          0       //Set to 1 to generate a self-test fault
#define INTERRUPT_TEST_TIME             819u    // Interrupt test period should be about 25msec
#define NUMBER_OF_TIMER_1_TICKS_LO      24u     // Lowest acceptible interrupt count based on 1kHz
#define NUMBER_OF_TIMER_1_TICKS_HI		26u     // Highest acceptible interrupt count based on 1kHz

#define ENABLE_EXIT_TIMEBASE            0       //When the exit timebase function is needed set to true
    
/************************ local data type definitions ************************/

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/
static u32 ulSftInterruptCnt        = 0;    //Holds the amount of counts
static u32 ulSftSysTimer1_Origin    = 0;    //Saves the origin value from the timer
static u32 ulSftSysTimer1_Delta     = 0;    //Saves the time between the measurements
static u32 ulSftSystemCnt_Origin    = 0;    //Holds the system count
static u32 ulSftSystemCnt_Delta     = 0;    //Holds the system count delta between the measurements

    
/************************ export data (const and var) ************************/


/****************************** local functions ******************************/
//********************************************************************************
/*!
\author     Kraemer E
\date       28.11.2020
\brief      Initializes the System Timer 1 for a LFCLK Timebase.
\return     none
\param      none
***********************************************************************************/
static void TimeBase_Entry(void)
{    
    if(HAL_WDT_SystemTimerGetRunningStatus(eSysTimer1) == eSysTimer_TimerSuspended)
    {
        HAL_WDT_SystemTimerInitFreeRunning(eSysTimer1);
    }
}

#if ENABLE_EXIT_TIMEBASE
//********************************************************************************
/*!
\author     Kraemer E
\date       28.11.2020
\brief      Disables the System Timer 1 for a further handling in the System
\return     none
\param      none
***********************************************************************************/
static void TimeBase_Exit(void)
{
    /* Check first if timer is running */
    if(HAL_WDT_SystemTimerGetRunningStatus(eSysTimer1) == eSysTimer_TimerRunning)
    {
        /* When running disable timer */
        HAL_WDT_SystemTimerDisableEnable(eSysTimer1, OFF);
    }
}
#endif

//********************************************************************************
/*!
\author     Kraemer E
\date       28.11.2020
\brief      Counts up the Self test interrupt counter
\return     none
\param      none
***********************************************************************************/
static void TimeBase_Interrupt(void)
{
    /* Increment interrupt counter. Each count is equal to a millisecond intervall */   
    ulSftInterruptCnt++;
}

/************************ externally visible functions ***********************/

//********************************************************************************
/*!
\author     Kraemer E
\date       28.11.2020
\brief      Timebase test. This test shall only be tested on start-up because
            it will consume 25ms test time.
\return     OK_STATUS, ERROR_STATUS
\param      none
***********************************************************************************/
u8 HAL_SelfTest_TimeBase_StartUp(void)
{
    /* Call entry function to initialize the System Timer */
    TimeBase_Entry();
    
    ulSftInterruptCnt = 0;                      // 1 mS Interrupt counter ( Source is HF Clock )

    //CySysWdtResetCounters(CY_SYS_WDT_COUNTER1_RESET);     //Reset WDT1 counter (Use LF Clock as source)
    ulSftSysTimer1_Origin = HAL_WDT_SystemTimerGetTick(eSysTimer1);     // Use SysTimer1 as a free runnung up counter, counting with ca. 32.768 Hz
    
    //Stop the 1ms System timer and route the interrupt callback to the new ISR
    HAL_Timer_Stop();                           // Stop and reset timer                          
    HAL_Timer_Init(TimeBase_Interrupt);         // Start the timer and set the callback   

    CyGlobalIntEnable;                          // Enable interrupts
    do                                          // Wait
    {
        ulSftSysTimer1_Delta = CySysTimerGetCount(CY_SYS_TIMER1) - ulSftSysTimer1_Origin;     // An overflow will never occur here after reset
    }
    while(ulSftSysTimer1_Delta < INTERRUPT_TEST_TIME);  // Wait ((1/32768Hz) * 819 ) = ca. 25 mS
    
    HAL_Timer_Stop();                           //Stop the system timer  
    
    /* Don't disable the WDT-System counter */
    #if ENABLE_EXIT_TIMEBASE
    TimeBase_Exit();
    #endif

    CyGlobalIntDisable;                         // No more interrupts during startup test
    
   	// If less than NUMBER_OF_TIMER_1_TICKS_LO or more then NUMBER_OF_TIMER_1_TICKS_HI ticks - error in test
	if((ulSftInterruptCnt < NUMBER_OF_TIMER_1_TICKS_LO) || (ulSftInterruptCnt > NUMBER_OF_TIMER_1_TICKS_HI))
   	{
		return ERROR_STATUS;
	}
    else
    {
        return OK_STATUS;
    }	
}
//********************************************************************************
/*!
\author     Kraemer E
\date       28.11.2020
\brief      Initialize the counts for the next test step.
\return     OK_STATUS
\param      none
***********************************************************************************/
u8 HAL_SelfTest_TimeBase_CyclicInit(void)
{
    /* Save active interrupts first */
    HAL_System_EnterCriticalSection();
    
    ulSftSysTimer1_Origin = HAL_WDT_SystemTimerGetTick(eSysTimer1);
    ulSftSystemCnt_Origin = HAL_Timer_GetTickCount();
    
    /* Enable interrupts again */
    HAL_System_LeaveCriticalSection();
    
    return OK_STATUS;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       28.11.2020
\brief      This test is called cyclical and checks for correct time bases.
\return     OK_STATUS, ERROR_STATUS, PENDING_STATUS
\param      none
***********************************************************************************/
u8 HAL_SelfTest_TimeBase_CyclicTest(void)
{
    u8 ucTestResult = OK_STATUS;
    
    /* Save active interrupts first */
    HAL_System_EnterCriticalSection();
    
    /* Get current WDT-System_Timer count */
    ulSftSysTimer1_Delta = HAL_WDT_SystemTimerGetTick(eSysTimer1) - ulSftSysTimer1_Origin;
    
    /* Get system tick value */
    ulSftSystemCnt_Delta = HAL_Timer_GetTickCount() - ulSftSystemCnt_Origin;
    
    /* Overflow found. Test can't be checked correctly */
    HAL_System_LeaveCriticalSection();
    
    /* Check for timer overflow */
    if((s32)ulSftSysTimer1_Delta < 0)
    {
        /* Overflow found. Test can't be checked correctly */
        return ucTestResult;
    }
    
    if((s32)ulSftSystemCnt_Delta < 0)
    {
        /* Overflow found. Test can't be checked correctly */
        return ucTestResult;
    }
    
    /* Check if test time of ca. 25ms has been reached */
    if(ulSftSysTimer1_Delta < INTERRUPT_TEST_TIME)
    {
        ucTestResult = PENDING_STATUS;
    }
    else
    {
        /* Check for low limits */
        if(ulSftSystemCnt_Delta < NUMBER_OF_TIMER_1_TICKS_LO)
        {
            ucTestResult = ERROR_STATUS;
        }
    }    
    return ucTestResult;
}
#endif //#ifdef HAL_SELFTEST_TIMEBASE

#endif //PSOC_4100S_PLUS