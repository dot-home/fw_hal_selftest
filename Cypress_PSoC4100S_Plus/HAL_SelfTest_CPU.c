/* *****************************************************************************
* File Name: HAL_SelfTest_CPU.c
* Version 1.1
*
* Description:
*  This file provides the source code to the API for runtime memory self tests for 
*  PSoC 4

* Related Document: 
*  AN89056: PSoC 4 - IEC 60730 Class B and IEC 61508 Safety Software Library
*
* Hardware Dependency:
*  CY8C40XX, CY8C42XX, CY8C42XXM, CY8C42XXL, CY8C40XXS, CY8C41XXS and 
*  CY8C41XXS Plus Devices
*******************************************************************************/

#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS

/********************************* includes **********************************/
#include <project.h>
#include "CyLib.h"
#include "HAL_SelfTest_CPU.h"
#include "BaseTypes.h"
#include "HAL_System.h"

#ifdef HAL_SELFTEST_CPU
    
/***************************** defines / macros ******************************/
/***************************************
* Initial Parameter Constants 
***************************************/
#define STEST_PATTERN_55        0x55u
#define STEST_PATTERN_AA        0xAAu

#define SELF_TEST_A_1           0x5Au
#define SELF_TEST_B_1           0xBF51u
#define SELF_TEST_C_1           0xCA82D3F8u

#define SELF_TEST_A_2           0xF5u
#define SELF_TEST_B_2           0x5CF1u
#define SELF_TEST_C_2           0x72F6C4B5u
/************************ local data type definitions ************************/

/************************* local function prototypes *************************/

static u8 SelfTest_PC5555( void ) CY_SECTION(".PCxx55");
static u8 SelfTest_PCAAAA( void ) CY_SECTION(".PCxxAA");

/************************* local data (const and var) ************************/
// Variables used in cpu PC tests
static volatile u8  CPU_PCTest_A;
static volatile u16 CPU_PCTest_B;
static volatile u32 CPU_PCTest_C;

/************************ export data (const and var) ************************/


/****************************** local functions ******************************/
//********************************************************************************
/*!
\author     Kraemer E.
\date       20.01.2021
\brief      Test Cpu Programm Counter
            Depends on placement of the routine at critical PC values x..x55 and x..xAA
            This functions performs tests for the PC register. Functions are placed at 0x1AAAA
            address and 0x15555 in flash. This function must return value 0xAA and 0x55 and
            write unique value to SelfTestA, SelfTestB and SelfTestC variables if PC is working
            correctly.
\return     STEST_PATTERN_55
*  SelfTestA:  write unique value to variable for this function SELF_TEST_A_1.
*  SelfTestB:  write unique value to variable for this function SELF_TEST_B_1.
*  SelfTestC:  write to unique value to variable for this function SELF_TEST_C_1.
\param      none
**********************************************************************************/
static u8 SelfTest_PC5555( void )
{
    CPU_PCTest_A = SELF_TEST_A_1;
    CPU_PCTest_B = SELF_TEST_B_1;
    CPU_PCTest_C = SELF_TEST_C_1;

    #if SET_ERROR_IN_CPUPC
        return (STEST_PATTERN_55 + 1u);     // Test on PC Test: Return error value
    #else
        return STEST_PATTERN_55;
    #endif
}

//********************************************************************************
/*!
\author     Kraemer E.
\date       20.04.2021
\brief      Test Cpu Programm Counter
            Depends on placement of the routine at critical PC values x..x55 and x..xAA
            This functions performs tests for the PC register. Functions are placed at 0x1AAAA
            address and 0x15555 in flash. This function must return value 0xAA and 0x55 and
            write unique value to SelfTestA, SelfTestB and SelfTestC variables if PC is working
            correctly.
\return     STEST_PATTERN_AA
*  SelfTestA:  write unique value to variable for this function SELF_TEST_A_2.
*  SelfTestB:  write unique value to variable for this function SELF_TEST_B_2.
*  SelfTestC:  write to unique value to variable for this function SELF_TEST_C_2.
\param      none
**********************************************************************************/
static u8 SelfTest_PCAAAA( void )
{
    CPU_PCTest_A = SELF_TEST_A_2;
    CPU_PCTest_B = SELF_TEST_B_2;
    CPU_PCTest_C = SELF_TEST_C_2;

    #if SET_ERROR_IN_CPUPC
        return (STEST_PATTERN_AA + 1u);             // Test on PC Test: Return error value
    #else
        return STEST_PATTERN_AA;
    #endif // End ERROR_IN_PROGRAM_COUNTER
}

/************************ externally visible functions ***********************/
//********************************************************************************
/*!
\author  Kraemer E
\date    13.04.2021
\brief  This function performs the test of the PC register. Function calls 
        SelfTest_PC5555() and SelfTest_PCAAAA() which are placed at 0x1555 and 0x2AAA
        addresses in flash for PSoC 4000. At 0x5555 and 0x2AAA addresses in flash for 
        PSoC 4200, PSoC 4000S. At 0x5555 and 0xAAAA addresses in flash for PSoC 4100S.
        At 0x15555 and 0xAAAA addresses in flash for PSoC 4100S Plus, PSoC 4M. 
        At 0x15555 and 0x2AAAA addresses in flash for PSoC 4200L. 
        These functions must return unique values to SelfTestA, SelfTestB and 
        SelfTestC variables if PC is working correctly.
\param   none
\return  none
***********************************************************************************/
u8 HAL_SelfTest_CPU_PC(void)
{     
    u8 uRet = ERROR_STATUS;    //Is equal to eSelfTest_ERROR
	u8 uReturnedValue;

    CPU_PCTest_A = 0x00u;
	CPU_PCTest_B = 0x0000u;
	CPU_PCTest_C = 0x00000000u;

    /* Disable interrupts */
    HAL_System_EnterCriticalSection();
    
    uReturnedValue = SelfTest_PC5555();
    
	// Check if SelfTest_PC5555() returned and wrote correct values to global variables
	if( (CPU_PCTest_A == SELF_TEST_A_1) && (CPU_PCTest_B == SELF_TEST_B_1) && (CPU_PCTest_C == SELF_TEST_C_1) )
	{
		if(uReturnedValue == STEST_PATTERN_55)
		{
			uReturnedValue = SelfTest_PCAAAA();
	        
			// Check if SelfTest_PCxxAA() returned and wrote correct values to global variables
			if( (CPU_PCTest_A == SELF_TEST_A_2) && (CPU_PCTest_B == SELF_TEST_B_2) && (CPU_PCTest_C == SELF_TEST_C_2) )
			{
				if ( uReturnedValue == STEST_PATTERN_AA )
				{   
					uRet = OK_STATUS; //Is equal to eSelfTest_OK
				}    
			}
		}
	}
    
    /* Enable interrupts */
    HAL_System_LeaveCriticalSection();
    
    return uRet;	
}





//********************************************************************************
/*!
\author  Kraemer E
\date    13.04.2021
\brief   Test the CPU register. This function performs checkerboard test for 
         all CPU registers except the PC register.
\param   none
\return  ucReturnValue - "0" - pass test; "1" - fail test.
***********************************************************************************/
u8 HAL_SelfTest_CPU_Reg(void)
{
    /* Disable interrupts */
    HAL_System_EnterCriticalSection();
    
    u8 ucReturnValue = ERROR_STATUS;    
    ucReturnValue = SelfTest_CPU_Regs_GCC();	
    
    /* Enable interrupts */
    HAL_System_LeaveCriticalSection();
    
    return ucReturnValue;	
}

#endif 

#endif //PSOC_4100S_PLUS
