/* *****************************************************************************
* File Name: SelfTest_Stack.c
* Version 1.1
*
* Description:
*  This file provides the source code to the API for runtime Stack self tests for 
*  PSoC 4
*
* Owner:
*  JOBI
* 
* Related Document: 
*  AN89056: PSoC 4 - IEC 60730 Class B and IEC 61508 Safety Software Library
*
* Hardware Dependency:
*  CY8C40XX, CY8C42XX, CY8C42XXM, CY8C42XXL, CY8C40XXS, CY8C41XXS and 
*  CY8C41XXS Plus Devices
*******************************************************************************/

#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS

/********************************* includes **********************************/
#include "HAL_SelfTest_Stack.h"
#include "CyLib.h"
//#include "TestConfiguration.h"

#ifdef HAL_SELFTEST_STACK

/***************************** defines / macros ******************************/
/***************************************
* Initial Parameter Constants 
***************************************/

/* Stack test parameters */
#define STACK_TEST_PATTERN        0x55AAu

/* Block size to be tested. Should be EVEN*/
#define STACK_TEST_BLOCK_SIZE     0x08u

/* PSoC memory parameters */
#define PSOC_SRAM_SIZE            CYDEV_SRAM_SIZE
#define PSOC_SRAM_BASE            CYDEV_SRAM_BASE

/* PSoC stack parameters */
#define PSOC_STACK_BASE			  (CYDEV_SRAM_BASE + CYDEV_SRAM_SIZE)
#define PSOC_STACK_SIZE			  (CYDEV_STACK_SIZE)
#define PSOC_STACK_END			  ((uint32)(PSOC_STACK_BASE - PSOC_STACK_SIZE))

#define ERROR_IN_STACK      0u      //Set to 1 to set a fault pattern into the stack       

/************************ local data type definitions ************************/

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/


/****************************** local functions ******************************/


/************************ externally visible functions ***********************/
//********************************************************************************
/*!
\author     Kraemer E
\date       29.11.2020
\brief      This function initializes the upper stack area with 0xAA and 0x55 pattern.
\return     none
\param      none
***********************************************************************************/
void HAL_SelfTest_Stack_Init(void)
{
	/* Pointer to the last word in the stack*/
	u16* uiStackAddr = (u16*)PSOC_STACK_END;
		
	/* Fill test stack block with predefined pattern */
    u8 ucStIdx;    
	for(ucStIdx = 0u; ucStIdx < (STACK_TEST_BLOCK_SIZE / sizeof(u16)); ucStIdx++)
	{
		#if (ERROR_IN_STACK)
			*stack = STACK_TEST_PATTERN + 1u;
            stack++;
		#else
			*uiStackAddr = STACK_TEST_PATTERN;
            uiStackAddr++;
		#endif /* End (ERROR_IN_STACK) */
	}
}

//********************************************************************************
/*!
\author     Kraemer E
\date       29.11.2020
\brief      This function performs stack self test. It checks upper stack area for 0xAA 
            and 0x55 pattern.
\return     Result of test:  "0" - pass test; "1" - fail test.
\param      none
***********************************************************************************/
u8 HAL_SelfTest_Stack_Check(void)
{
	u8 ucRetVal = OK_STATUS;
		
	/* Pointer to the last word in the stack */
	u16* uiStackAddr = (u16*)PSOC_STACK_END;
		
	/* Check test stack block for pattern and return error if no pattern found */
    u8 ucStIdx;
	for(ucStIdx = 0u; ucStIdx < (STACK_TEST_BLOCK_SIZE / sizeof(u16)); ucStIdx++)
	{
		if(*uiStackAddr != STACK_TEST_PATTERN)
		{
            uiStackAddr++;
			ucRetVal = ERROR_STATUS;
			break;
		}
	}
		
	return ucRetVal;
}

#endif //#ifdef HAL_SELFTEST_STACK

#endif //PSOC_4100S_PLUS