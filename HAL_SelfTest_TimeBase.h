#ifndef _HAL_SELFTEST_TIMEBASE_H_
#define _HAL_SELFTEST_TIMEBASE_H_
    
#include "BaseTypes.h"
#include "SelfTest_Config.h"
    
#ifdef HAL_SELFTEST_TIMEBASE
    
/*************************************** 
* Function Prototypes 
***************************************/
uint8_t HAL_SelfTest_TimeBase_StartUp(void);
uint8_t HAL_SelfTest_TimeBase_CyclicInit(void);
uint8_t HAL_SelfTest_TimeBase_CyclicTest(void);

/***************************************
* Initial Parameter Constants 
***************************************/
/* Status flags to return function results */
#if !defined(PENDING_STATUS)
    #define PENDING_STATUS  	           (2u)
#endif /* End PENDING_STATUS) */

/* Status flags to return function results */
#if !defined(ERROR_STATUS)
    #define ERROR_STATUS  	           (1u)
#endif /* End ERROR_STATUS) */

#if !defined(OK_STATUS)
    #define OK_STATUS   		       (0u)
#endif /* End OK_STATUS) */

#endif //#ifdef HAL_SELFTEST_TIMEBASE

#endif /* _HAL_SELFTEST_TIMEBASE_H_ */


/* END OF FILE */
