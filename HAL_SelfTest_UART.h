/*******************************************************************************
* File Name: HAL_SelftTest_UART.h
*  Version 1.1
*
* Description:
*  This file provides constants and parameter values used for Stack    
*  self tests for PSoC 4
*
* Owner:
*  JOBI
* 
* Related Document: 
*  AN89056: PSoC 4 - IEC 60730 Class B and IEC 61508 Safety Software Library
*
* Hardware Dependency:
*  CY8C40XX, CY8C42XX, CY8C42XXM, CY8C42XXL, CY8C40XXS, CY8C41XXS and 
*  CY8C41XXS Plus Devices
*******************************************************************************/
#ifndef _HAL_SELFTEST_UART_H_
#define _HAL_SELFTEST_UART_H_

#include "BaseTypes.h"
#include "SelfTest_Config.h"
    
#ifdef HAL_SELFTEST_UART
    
/*************************************** 
* Function Prototypes 
***************************************/
void  HAL_SelfTest_UART_Init(void);
uint8 HAL_SelfTest_UART_Check(void);


/* Status flags to return function results */
#if !defined(ERROR_STATUS)
    #define ERROR_STATUS  	           (1u)
#endif /* End ERROR_STATUS) */

#if !defined(OK_STATUS)
    #define OK_STATUS   		       (0u)
#endif /* End OK_STATUS) */

#if !defined(PASS_STILL_TESTING_STATUS)
    #define PASS_STILL_TESTING_STATUS   (2u) 
#endif /* End PASS_STILL_TESTING_STATUS */

#if !defined(PASS_COMPLETE_STATUS)
    #define PASS_COMPLETE_STATUS        (3u) 
#endif /* End PASS_COMPLETE_STATUS */

#if !defined(ERROR_TX_NOT_EMPTY)
    #define ERROR_TX_NOT_EMPTY          (4u)
#endif /* End ERROR_TX_NOT_EMPTY */

#if !defined(ERROR_RX_NOT_EMPTY)
    #define ERROR_RX_NOT_EMPTY          (5u)
#endif /* End ERROR_RX_NOT_EMPTY */   

#if !defined(ERROR_RX_TX_NOT_ENABLE)
    #define ERROR_RX_TX_NOT_ENABLE         (6u)
#endif /* End ERROR_RX_TX_NOT_ENABLE */  
  
#if !defined(UNKNOWN_ERROR)
    #define UNKNOWN_ERROR               (8u)
#endif /* End UNKNOWN_ERROR */

#endif // HAL_SELFTEST_UART

#endif /* _HAL_SELFTEST_UART_H_ */


/* END OF FILE */
