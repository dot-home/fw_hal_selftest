/*******************************************************************************
* File Name: HAL_SelfTest_CPU.h
*  Version 1.1
*
* Description:
*  This file provides constants and parameter values used for Stack    
*  self tests for PSoC 4
*
* Owner:
*  JOBI
* 
* Related Document: 
*  AN89056: PSoC 4 - IEC 60730 Class B and IEC 61508 Safety Software Library
*
* Hardware Dependency:
*  CY8C40XX, CY8C42XX, CY8C42XXM, CY8C42XXL, CY8C40XXS, CY8C41XXS and 
*  CY8C41XXS Plus Devices
*******************************************************************************/
#ifndef _HAL_SELFTEST_CPU_H_
#define _HAL_SELFTEST_CPU_H_

#include "BaseTypes.h"
#include "SelfTest_Config.h"

#ifdef HAL_SELFTEST_CPU
    
/*************************************** 
* Function Prototypes 
***************************************/
u8 HAL_SelfTest_CPU_PC(void);
u8 HAL_SelfTest_CPU_Reg(void);
u8 SelfTest_CPU_Regs_GCC(void);


/* Status flags to return function results */
#if !defined(ERROR_STATUS)
    #define ERROR_STATUS  	           (1u)
#endif /* End ERROR_STATUS) */

#if !defined(OK_STATUS)
    #define OK_STATUS   		       (0u)
#endif /* End OK_STATUS) */

#define CPU_OK         	0x00	
#define CPU_FAILURE	    0x01  

#endif //#ifdef HAL_SELFTEST_CPU

#endif /* _HAL_SELFTEST_CPU_H_ */


/* END OF FILE */
